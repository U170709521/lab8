package shapes3d;

public class TestShapes3d {
	public static void main(String args[]) {
		Cube cube = new Cube(3.0);
		Cylinder cylinder = new Cylinder(3.0, 3.0);
		System.out.println("Area of " + cube + "=" + cube.area());
		System.out.println("Volume of " + cube + "=" + cube.volume());
		System.out.println("Area of " + cylinder + "=" + cylinder.area());
		System.out.println("Volume of " + cylinder + "=" + cylinder.volume());
	}
}
