package shapes2d;

public class Circle {
	private double radius;

	public Circle() {
		radius = 1.0;
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double d) {
		this.radius = d;
	}

	public double area() {
		return Math.PI * radius * radius;
	}

	public String toString() {
		return "Circle[radius=" + radius+"]";
	}
}
