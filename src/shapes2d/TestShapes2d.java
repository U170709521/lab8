package shapes2d;

import shapes3d.Cube;

public class TestShapes2d {
	public static void main(String args[]) {
		Square square = new Square(3.0);
		Circle circle = new Circle(3.0);
		System.out.println("Area of " + square + "=" + square.area());
		System.out.println("Area of " + circle + "=" + circle.area());
	}
}
